<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QAndAModel extends Model
{
    protected $table = 'qanda';

    protected $fillable = ['answer', 'question'];

    public function result() {
        return $this->hasOne('App\ResultModel', 'question_id');
    }
}
