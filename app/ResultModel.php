<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultModel extends Model
{
    protected $table = 'result';

    public function question() {
        return $this->belongsTo('App\QAndAModel', 'question_id');
    }
}
