<?php

namespace App\Console\Commands;

use App\QAndAModel;
use App\ResultModel;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class QAndA extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qanda:interactive {action="Add Question"}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs an interactive command line based Q And A system.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->menu();
    }

    public function menu() {
        $this->line(' ');

        if($this->hasArgument('action')) {
            $action = $this->argument('action');
        } else {
            $action = $this->choice('What whould you like to do?', [
                'Add Question','View Questions', 'Practice', 'View Results', 'Exit'
            ]);
        }

        switch ($action) {
            case 'Add Question':
                $this->addQuestion();
                break;
            case 'View Questions':
                $this->viewQuestions();
                break;
            case 'Practice':
                $this->practice();
                break;
            case 'View Results':
                $this->viewResults();
                break;
        }
    }

    public function shouldSave(Model $model) {
        $canSave = $this->confirm('Are you sure you want do it?', false);

        if($canSave) {
            return $model->save();
        }

        return false;
    }

    public function addQuestion() {
        $question = $this->ask('Please, enter a question');

        $answer = $this->ask('Now, the answer');

        $question = new QAndAModel([
            'question' => Str::endsWith($question, '?') === false
                ? $question . '?'
                : $question,
            'answer' => $answer]);

        $saved = $this->shouldSave($question);

        if($saved) {
            $this->info('Question stored!');
            return $this->menu();
        }

        return $this->addQuestion();
    }

    public function practice() {
        $questions = QAndAModel::doesntHave('result')->get();

        if($questions->count() === 0) {
            $this->info('No questions left');
            return $this->viewResults();
        }

        $chosen = $this->choice('Please, choose a question', $questions->pluck('question')->toArray());

        $question = $questions->where('question', $chosen)->first();

        $answer = $this->ask(Str::endsWith($question->question, '?') === false
            ? $question->question . '?'
            : $question->question
        );

        $result = new ResultModel();

        $result->answer = $answer;
        $result->correct = Str::lower($answer) == Str::lower($question->answer);

        $result->question()->associate($question);

        $saved = $this->shouldSave($result);

        if($saved) {
            $this->info('Answer stored!');
            return $this->viewResults();
        }

        return $this->practice();
    }

    public function viewQuestions() {
        $headers = ['Question', 'Answer'];

        $questions = QAndAModel::all(['question', 'answer'])->toArray();

        $this->table($headers, $questions);

        return $this->menu();
    }

    public function viewResults() {
        $headers = ['Question', 'Your Answer', 'Result'];

        $results = QAndAModel::with('result')->get();

        $this->line(' ');

        if($results->count() === 0) {
            $this->info('No results found');
            return $this->menu();
        }

        $rows = $results->map(function($item) {
            return [
                $item->question,
                object_get($item, 'result.answer', '--'),
                object_get($item, 'result.correct') == 1 ? 'Correct' : 'Incorrect'
            ];
        });

        $this->table($headers, $rows);
        $this->line(' ');
        $this->info('Progress');
        $this->line(' ');
        $bar = $this->output->createProgressBar(QAndAModel::all()->count());

        foreach ($rows as $result) {
            if(array_get($result, 2) == 'Correct') {
                $bar->advance();
            }
        }

        return $this->menu();
    }
}
