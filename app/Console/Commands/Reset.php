<?php

namespace App\Console\Commands;

use App\ResultModel;
use Illuminate\Console\Command;

class Reset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qanda:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes all previous progresses';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ResultModel::truncate();

        $this->info('Progress erased');
    }
}
