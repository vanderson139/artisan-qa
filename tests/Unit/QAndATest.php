<?php

namespace Tests\Unit;

use Mockery;
use Tests\TestCase;

class QAndATest extends TestCase
{
    /**
     * @return void
     */
    public function testItShowsTheMainMenu()
    {
        $command = Mockery::mock('App\Console\Commands\QAndA[menu]');
        $this->app[\App\Console\Commands\QAndA::class] = $command;

        $command->shouldReceive('menu')->once();

        $this->artisan('qanda:interactive', ['--no-interaction' => true]);
    }

    /**
     * @return void
     */
    public function testItCallsTheAddQuestionMethod()
    {
        $command = Mockery::mock('App\Console\Commands\QAndA[addQuestion]');
        $this->app[\App\Console\Commands\QAndA::class] = $command;

        $command->shouldReceive('addQuestion')->once();

        $this->artisan('qanda:interactive', ['action' => 'Add Question', '--no-interaction' => true]);
    }

    /**
     * @return void
     */
    public function testItCallsTheViewQuestionsMethod()
    {
        $command = Mockery::mock('App\Console\Commands\QAndA[viewQuestions]');
        $this->app[\App\Console\Commands\QAndA::class] = $command;

        $command->shouldReceive('viewQuestions')->once();

        $this->artisan('qanda:interactive', ['action' => 'View Questions', '--no-interaction' => true]);
    }

    /**
     * @return void
     */
    public function testItCallsThePracticeMethod()
    {
        $command = Mockery::mock('App\Console\Commands\QAndA[practice]');
        $this->app[\App\Console\Commands\QAndA::class] = $command;

        $command->shouldReceive('practice')->once();

        $this->artisan('qanda:interactive', ['action' => 'Practice', '--no-interaction' => true]);
    }

    /**
     * @return void
     */
    public function testItCallsTheViewResultsMethod()
    {
        $command = Mockery::mock('App\Console\Commands\QAndA[viewResults]');
        $this->app[\App\Console\Commands\QAndA::class] = $command;

        $command->shouldReceive('viewResults')->once();

        $this->artisan('qanda:interactive', ['action' => 'View Results', '--no-interaction' => true]);
    }
}
